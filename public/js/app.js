(function ($) {
    "use strict";

    var $components = [];

    document.addEventListener("DOMContentLoaded", function(){
        var component;
        for(component in $components){
            $components[component]();
        }
    });

	$components.input = function () {
		var inputs = Array.from(document.querySelectorAll('.app-form-item input'));
		var onInput = function() {
			this.classList.toggle("active", this.value.length > 0)
		};
		inputs.map(function(e){
			e.addEventListener('input', onInput);
		})
	};

    $components.mobileMenu = function () {

		var activeMenu;

		var header = document.querySelector(".app-header");
		var menu = document.querySelector("#mobile-menu");
		var button = document.querySelector("#mobile-menu-button");


		var closeMenu = function (){
			document.body.style.overflow = null;
			menu.style.display = null;
			header.classList.remove("mobile-menu-active");
			if(activeMenu !== undefined) {
				$(activeMenu).fadeOut();
			}
		};

		var toogleMenu = function () {
			if (header.classList.contains('mobile-menu-active') && this === button) {
				closeMenu();
				return;
			}
			header.classList.add("mobile-menu-active");
			document.body.style.overflow = 'hidden';
			activeMenu = menu;
			$(menu).fadeIn();
		};

		window.addEventListener("orientationchange", closeMenu);
		window.addEventListener("resize", closeMenu);
		button.addEventListener('click', toogleMenu);
	};

    $components.sideBar = function () {
        var headerMobileMenuButton = document.querySelector("#mobile-menu-button");
		headerMobileMenuButton.addEventListener('click', function(){
			this.classList.toggle("active")
		});
    };

    $components.homeTitleAnimation = function () {
		var $subtitle = $('.js-animation-subtitle');
		var interval = 3000;
		var start = performance.now();
		var transition = 500;

		var req = requestAnimationFrame(animate);

		function animate(timestamp) {
			var $prev = $subtitle.children().eq(0);
			var $next = $subtitle.children().eq(1);
			var progress = timestamp - start;

			if (progress > interval) {
				$prev.css({
					display: 'block',
					transform: 'translateY(-100%)',
					transition: transition + 'ms'
				});

				setTimeout(function () {
					$prev.appendTo($subtitle);
					$prev.attr('style', 'display: none;');
					$next.removeAttr('style');
				}, transition);

				$next.css('display', 'block');

				setTimeout(function () {
					$next.css({
						transform: 'translateY(-100%)',
						transition: transition + 'ms'
					});
				}, 0);

				start = timestamp + transition;
			}
			req = requestAnimationFrame(animate);
		}
    };


    $components.modalFormRequiredCheckbox = function () {
    	var modal = document.querySelector(".app-modal-expert");

    	if(modal){
			var checkboxes = modal.querySelectorAll("._form-submit input[type=checkbox].js-required");
			var submit = modal.querySelector("._form-submit .app-button-green");

			var changeButtonVisibility = function () {
				submit.classList.toggle("app-hidden", !isModalAgreementsChecked.call(this))
			};

			var handler = function() {
				changeButtonVisibility();
			};

			Array.from(checkboxes).map(function(el) {
				el.addEventListener("change", handler);
			});

			var isModalAgreementsChecked = function() {
				return Array.from(checkboxes).filter(function(el) {
					return el.checked === true;
				}).length > 0;

			};

			changeButtonVisibility();
		}
    };

}(jQuery));
