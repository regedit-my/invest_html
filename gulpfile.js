(function(r){
    var gulp = r("gulp");
    var watch = r('gulp-watch');
    var sass = r('gulp-sass');
    var includer = r("gulp-swig");
    var iconfont = r('gulp-iconfont');
    var iconfontCss = r('gulp-iconfont-css');

    var fontName = 'svg-fonts';

    gulp.task('scss', function () {
        return gulp.src("./workflow/scss/*.scss")
            .pipe(sass({
                outputStyle: 'compressed',
                compass: true,
                bundleExec: true
            }).on('error', sass.logError))
            .pipe(gulp.dest('./public/css/'));
    });

    gulp.task('includer', function() {
        gulp.src(['./workflow/html/*.html'])
            .pipe(includer({defaults: { cache: false }}))
            .pipe(gulp.dest('./public/'))
    });

    gulp.task('iconfont', function(){
        gulp.src(['workflow/icons/svg/*.svg'])
            .pipe(iconfontCss({
                fontName: fontName,
                path: 'workflow/icons/template.scss',
                targetPath: '../../workflow/scss/slices/_icons.scss',
                fontPath: '../fonts/'
            }))
            .pipe(iconfont({
                fontName: fontName,
                formats: ['ttf', 'eot', 'woff', 'woff2'],
                // prependUnicode: true,
                normalize: true,
                fontHeight: 1001
            }))
            .pipe(gulp.dest('public/fonts/'));
    });

    gulp.task('watch', function () {
        gulp.watch('./workflow/scss/*.scss', ['scss']);
        gulp.watch('./workflow/scss/slices/*.scss', ['scss']);
        gulp.watch('./workflow/html/*.html', ['includer']);
        gulp.watch('./workflow/html/slices/*.html', ['includer']);
        gulp.watch('./workflow/html/components/*.html', ['includer']);

        gulp.watch('./workflow/svg/*.svg', ['iconfont']);
    });

})(require);
